package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

            // Setting up variables to keep track of:
            //  - Round-number: int roundNumber
            //  - Score: int humanScore, int computerScore
            //  - When to end: boolean cont
            //  - choices: String humanChoice, String computerChoice
            //  - is the input a valid choice?: boolean validChoice
        
            // String containing the users choice:
        int humanChoiceInt;
        String humanChoice;

            // An RNG for picking the computerChoice:
        Random rng = new Random();

        int computerChoiceInt;
        String computerChoice;

        String cont = "y"; // cont = "y" if the user wants to continue, and "n" otherwise.
        
            // Plays a round of rock-paper-scissors:
            //
        while (cont.equals("y")) {

                // Takes in the user's choice:
                //
            System.out.println("Let's play round " + roundCounter);
            humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");

                // Prompts the user again if the input is not a valid choice:
                //
            while (!rpsChoices.contains(humanChoice)) {
                System.out.print("I do not understand " + humanChoice + ". Could you try again? \n");
                humanChoice = readInput("Your choice (rock/paper/scissors)?");
            }

                // Converts the humanChoice into a number:
            if (humanChoice.equals("rock")){humanChoiceInt = 0;}
            else if (humanChoice.equals("paper")){humanChoiceInt = 1;}
            else {humanChoiceInt = 2;} // (humanChoice.equals("scissors")){humanChoiceInt = 2;}

            //System.out.println("humanChoice:" + humanChoice + ", humanChoiceInt:" + humanChoiceInt + "\n");
                // Randomly selects a choice for the computer:
                //
            computerChoiceInt = rng.nextInt(rpsChoices.size());
            computerChoice = rpsChoices.get(computerChoiceInt);

            //System.out.println("computerChoice:" + computerChoice + ", computerChoiceInt:" + computerChoiceInt + "\n");
            //System.out.println((humanChoiceInt + 3 - computerChoiceInt) % 3);
            

            if (humanChoiceInt == computerChoiceInt) {
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". It's a tie!");
            }

            else if (((3 + humanChoiceInt - computerChoiceInt) % 3) == 1) {
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                humanScore ++;
            }
            else {
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                computerScore ++;
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                // Asks they player if they want to play again:
            cont = readInput("Do you wish to continue playing? (y/n)?");

                // If cont isn't set to a valid option:
            while (!cont.equals("y") && !cont.equals("n")) {
                System.out.println("I do not understand " + cont + ". Could you try again?");
                cont = readInput("Do you wish to continue playing? (y/n)?");
            }

            if (cont.equals("y")) {roundCounter++;}
        }
        System.out.println("Bye bye :)");
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
